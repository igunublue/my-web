/**
 * @type {import('next').NextConfig}
 * */

const nextConfig = {
	reactStrictMode: true,
	compiler: {
		styledComponents: true,
	},
	eslint: {
		dirs: [
			'pages',
			'components',
			'lib',
			'common',
			'config',
			'hooks',
			'layout',
			'styles',
			'theme',
			'utils',
		],
	},
}

module.exports = nextConfig
