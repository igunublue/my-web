const path = require('path')

module.exports = {
	// Lint then format TypeScript and JavaScript files
	'**/*.(jsx|js)': (filenames) => [
		`pnpm next lint --fix --file ${filenames
			.map((f) => path.relative(process.cwd(), f))
			.join(' --file ')}`,
		`pnpm eslint --fix ${filenames.join(' ')}`,
		`pnpm prettier --write ${filenames.join(' ')}`,
	],

	// Format MarkDown and JSON
	'**/*.(md|json)': (filenames) =>
		`pnpm prettier --write ${filenames.join(' ')}`,
}
