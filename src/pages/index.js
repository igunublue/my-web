import styled from 'styled-components'
import fs from 'fs'
import path from 'path'
import matter from 'gray-matter'

import {
	Hero,
	AboutMe,
	MyWork,
	Projects,
	Contact,
	// Blog
} from '@/components'
import dayJsPlus from '@/lib/dayJsPlus'
import { POSTS_PATH } from '@/lib/constants'

const StyledMainContainer = styled.section`
	width: 100%;
	max-width: 900px;
	counter-reset: section;
	section {
		margin: 0 auto;
		padding: 100px 0;
	}
`

// function IndexPage({ postsData }) {
function IndexPage() {
	return (
		<StyledMainContainer className='fillHeight'>
			<Hero />
			<AboutMe />
			<MyWork />
			<Projects />
			{/* <Blog postsData={postsData} itemsPerPage={5} /> */}
			<Contact />
		</StyledMainContainer>
	)
}

export const getStaticProps = async () => {
	const files = fs.readdirSync(POSTS_PATH)
	const postsData = files.map((nameWithExt) => {
		const markdownWithMetadata = matter(
			fs.readFileSync(path.join(POSTS_PATH, nameWithExt), 'utf8'),
		)

		return {
			slug: nameWithExt.replace('.md', ''),
			tags: markdownWithMetadata.data.tags,
			title: markdownWithMetadata.data.title,
			date: dayJsPlus(markdownWithMetadata.data.date).format('LL'),
		}
	})

	return {
		props: {
			postsData,
		},
	}
}

export default IndexPage
