import fs from 'fs'
import path from 'path'
import matter from 'gray-matter'

import { POSTS_PATH } from '@/lib/constants'
import { Contact, Post } from '@/components'

export default function PostPage({ content, title }) {
	return (
		<>
			<Post content={content} title={title} />
			<Contact />
		</>
	)
}

export const getStaticPaths = async () => {
	const files = fs.readdirSync(POSTS_PATH)
	const paths = files.map((filename) => ({
		params: { slug: filename.replace('.md', '') },
	}))
	return {
		paths,
		fallback: false,
	}
}

export const getStaticProps = async ({ params: { slug } }) => {
	const markdownWithMetadata = fs.readFileSync(
		path.join(POSTS_PATH, slug + '.md'),
		'utf8',
	)
	const parsedMarkdown = matter(markdownWithMetadata)

	return {
		props: {
			content: parsedMarkdown.content,
			title: parsedMarkdown.data.title,
		},
	}
}
