import { useEffect } from 'react'
import { ThemeProvider } from 'styled-components'

import DefaultLayout from '@/layouts/default'
import GlobalStyles from '@/styles/globals'
import theme from '@/themes/light'
import postHogInit from '@/analytics/postHog/utils/initInstance'

export default function App({ Component, pageProps }) {
	const Layout = Component.Layout || DefaultLayout

	useEffect(() => {
		postHogInit()
	}, [])

	return (
		<ThemeProvider theme={theme}>
			<Layout>
				<GlobalStyles />
				<Component {...pageProps} />
			</Layout>
		</ThemeProvider>
	)
}
