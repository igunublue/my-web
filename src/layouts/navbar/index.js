import { useState, useEffect, useRef } from 'react'
import PropTypes from 'prop-types'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { Link as ScrollLink, scroller } from 'react-scroll'
import { useRouter } from 'next/router'

import config from '@/config'
import { LOADER_DELAY } from '@/lib/constants'
import { useScrollDirection } from '@/hooks'
import { Menu } from '@/components'
import { IconLogo } from '@/components/Icons'
import { StyledHeader, StyledNav, StyledLinks } from './styles'

function Li({ isHome, index, name }) {
	const ref = useRef()
	const router = useRouter()
	const fadeDownClass = isHome ? 'fadedown' : ''
	const timeout = isHome ? LOADER_DELAY : 0

	const handleRedirect = (homeElement) => async () => {
		await router.push('/')

		return scroller.scrollTo(homeElement, {
			duration: 1000,
			smooth: 'easeInOutQuint',
		})
	}

	return (
		<CSSTransition classNames={fadeDownClass} timeout={timeout}>
			<li
				style={{ transitionDelay: `${isHome ? index * 100 : 0}ms` }}
				ref={ref}
			>
				{isHome ? (
					<ScrollLink spy={true} smooth={true} duration={1000} to={name}>
						{name}
					</ScrollLink>
				) : (
					<a ref={ref} onClick={handleRedirect(name)}>
						{name}
					</a>
				)}
			</li>
		</CSSTransition>
	)
}

const Nav = ({ isHome }) => {
	const { navLinks } = config
	const [isMounted, setIsMounted] = useState(!isHome)
	const scrollDirection = useScrollDirection('down')
	const [scrolledToTop, setScrolledToTop] = useState(true)
	const handleScroll = () => {
		setScrolledToTop(window.pageYOffset < 50)
	}
	const timeout = isHome ? LOADER_DELAY : 0
	const fadeClass = isHome ? 'fade' : ''

	useEffect(() => {
		const timeout = setTimeout(() => {
			setIsMounted(true)
		}, 100)

		window.addEventListener('scroll', handleScroll)

		return () => {
			clearTimeout(timeout)
			window.removeEventListener('scroll', handleScroll)
		}
	}, [])

	return (
		<StyledHeader
			scrollDirection={scrollDirection}
			scrolledToTop={scrolledToTop}
		>
			<StyledNav>
				<TransitionGroup component={null}>
					{isMounted && (
						<CSSTransition classNames={fadeClass} timeout={timeout}>
							<div className='logo' tabIndex='-1'>
								{
									<ScrollLink
										href='/'
										spy={true}
										smooth={true}
										duration={1000}
										to='hero'
									>
										<IconLogo width={36.581} height={50.186} />
									</ScrollLink>
								}
							</div>
						</CSSTransition>
					)}
				</TransitionGroup>

				<StyledLinks>
					<ol>
						<TransitionGroup component={null}>
							{isMounted &&
								navLinks &&
								navLinks.map(({ name }, i) => (
									<Li key={name} isHome={isHome} index={i} name={name} />
								))}
						</TransitionGroup>
					</ol>
				</StyledLinks>

				<TransitionGroup component={null}>
					{isMounted && (
						<CSSTransition classNames={fadeClass} timeout={timeout}>
							<Menu />
						</CSSTransition>
					)}
				</TransitionGroup>
			</StyledNav>
		</StyledHeader>
	)
}

Nav.propTypes = {
	isHome: PropTypes.bool,
}

export default Nav
