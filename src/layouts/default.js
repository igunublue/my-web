import PropTypes from 'prop-types'
import { useState } from 'react'
import { useRouter } from 'next/router'

import { Loader, Social, Email } from '../components'
import { SkipToContentLink } from './styles'
import Main from './main'
import BaseLayout from './base'
import Navbar from './navbar'
import Footer from './footer'

const DefaultLayout = ({ children }) => {
	const router = useRouter()
	const isHome = router.pathname === '/'
	const [isLoading, setIsLoading] = useState(isHome)

	const handleFinish = () => setIsLoading(false)

	return (
		<BaseLayout>
			<>
				<SkipToContentLink href='#content'>Skip to Content</SkipToContentLink>
				{isLoading && isHome ? (
					<Loader onFinish={handleFinish} />
				) : (
					<>
						<Navbar isHome={isHome} />
						<Social isHome={isHome} />
						<Email isHome={isHome} />
						<Main id='content' className={isHome ? 'fillHeight' : ''}>
							{children}
						</Main>
						<Footer />
					</>
				)}
			</>
		</BaseLayout>
	)
}

DefaultLayout.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]),
}

export default DefaultLayout
