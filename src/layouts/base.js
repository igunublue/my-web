import Head from 'next/head'
import PropTypes from 'prop-types'

const BaseLayout = ({ children }) => {
	return (
		<div id='main'>
			<Head>
				<title>Lenin AGC | Fullstack DEV</title>
				<link rel='icon' href='/favicon.ico' />
				<link rel='manifest' href='/manifest.json' />
				<link
					rel='apple-touch-icon'
					sizes='180x180'
					href='/apple-touch-icon.png'
				/>
				<link
					rel='icon'
					type='image/png'
					sizes='32x32'
					href='/favicon-32x32.png'
				/>
				<link
					rel='icon'
					type='image/png'
					sizes='16x16'
					href='/favicon-16x16.png'
				/>
				<link rel='mask-icon' href='/safari-pinned-tab.svg' color='#181818' />
				<meta name='msapplication-TileColor' content='#ffffff' />
				<meta name='theme-color' content='#ffffff' />
				<meta
					name='viewport'
					key='viewport'
					content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'
				/>
				<meta
					property='og:site_name'
					content='Lenin Garizabalo | Fullstack DEV'
				/>
				<meta property='og:type' content='website' />
				<meta property='og:title' content='Lenin Garizabalo | Fullstack DEV' />
				<meta property='og:locale' content='en' />
				<meta property='og:url' content='https://lenin-agc.com' />
				<meta
					name='description'
					content="Hello! I'm Lenin, a Software Developer based in Barranquilla, Colombia. I enjoy creating beautiful and reliable applications for internet. My goal is to always build scalable products and performant experiences."
				/>
				<meta
					name='keywords'
					content='Developer, Javascript, Freelancer, React, JS Developer, React Developer'
				/>
				<meta property='og:image' content='https://lenin-agc.com/avatar.jpeg' />
				<meta
					property='twitter:image'
					content='https://lenin-agc.com/avatar.jpeg'
				/>
			</Head>
			{children}
		</div>
	)
}

BaseLayout.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node,
	]),
}

export default BaseLayout
