import React, { useState, useEffect, useCallback } from 'react'
import PropTypes from 'prop-types'
import anime from 'animejs'

import LogoLoader from '@/components/Icons/LogoLoader'
import { StyledLoader } from './styles'

const Loader = ({ onFinish }) => {
	const animate = useCallback(() => {
		const loader = anime.timeline({
			complete: () => onFinish(),
		})

		loader.add({
			targets: '#logo',
			delay: 1000,
			duration: 400,
			easing: 'easeInOutExpo',
			opacity: 0,
			scale: 0.4,
		})
	}, [onFinish])

	const [isMounted, setIsMounted] = useState(false)

	useEffect(() => {
		const timeout = setTimeout(() => setIsMounted(true), 10)
		animate()
		return () => clearTimeout(timeout)
	}, [animate])

	return (
		<StyledLoader className='loader' isMounted={isMounted}>
			<div className='logo-wrapper'>
				<LogoLoader />
			</div>
		</StyledLoader>
	)
}

Loader.propTypes = {
	onFinish: PropTypes.func.isRequired,
}

export default Loader
