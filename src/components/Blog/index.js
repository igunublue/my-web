import { useState } from 'react'
import { useRouter } from 'next/router'
import { Element, scroller } from 'react-scroll'

import {
	StyledMainContainer,
	CustomNumberHeading,
	Tag,
	TagContainer,
	PostContainer,
	Date,
	PaginateBlog,
} from './styles'
import config from '@/config'

function PostItems({ currentItems }) {
	const router = useRouter()

	const handleGoToPost = (slug) => () => router.push(`/post/${slug}`)

	return (
		<>
			{currentItems &&
				currentItems.map(({ slug, title, date, tags }) => (
					<PostContainer key={title + date}>
						<Date>{date}</Date>
						<CustomNumberHeading onClick={handleGoToPost(slug)}>
							{title}
						</CustomNumberHeading>
						<TagContainer>
							{tags.map((tag) => (
								<Tag className='project-overline' key={tag}>
									{tag}
								</Tag>
							))}
						</TagContainer>
					</PostContainer>
				))}
		</>
	)
}

export default function Blog({ postsData, itemsPerPage }) {
	const [itemOffset, setItemOffset] = useState(0)
	const endOffset = itemOffset + itemsPerPage
	const currentItems = postsData.slice(itemOffset, endOffset)
	const pageCount = Math.ceil(postsData.length / itemsPerPage)

	const handlePageClick = (event) => {
		scroller.scrollTo(config.navLinkNames.blog, {
			duration: 1000,
			smooth: 'easeInOutQuint',
		})
		const newOffset = (event.selected * itemsPerPage) % postsData.length
		return setItemOffset(newOffset)
	}

	return (
		<Element name={config.navLinkNames.blog}>
			<StyledMainContainer>
				<PostItems currentItems={currentItems} />
				<PaginateBlog
					breakLabel='...'
					nextLabel='next >'
					nextAriaLabel='button next page'
					previousAriaLabel='button previous page'
					onPageChange={handlePageClick}
					pageRangeDisplayed={5}
					pageCount={pageCount}
					previousLabel='< previous'
					renderOnZeroPageCount={null}
				/>
			</StyledMainContainer>
		</Element>
	)
}
