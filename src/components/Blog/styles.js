import styled from 'styled-components'
import ReactPaginate from 'react-paginate'

import { NumberedHeading } from '@/common/styles'

const marginLeft = '4.4rem'

export const StyledMainContainer = styled.section`
	width: 100%;
	max-width: 900px;
	min-height: 900px;
	counter-reset: section;
	margin-bottom: 15rem;
	section {
		margin: 0 auto;
		padding: 100px 0;
	}
`
export const CustomNumberHeading = styled(NumberedHeading)`
	margin: 0;
	margin-bottom: 1rem;
	margin-top: 0.5rem;
	white-space: pre-wrap;
	transition: 0.2s;
	&:hover {
		cursor: pointer;
		padding-left: 15px;
		transition: 0.2s;
	}
	&:after {
		display: none;
	}
`
export const PostContainer = styled.div`
	margin-bottom: 5rem;
`
export const TagContainer = styled.div`
	display: flex;
	flex-wrap: wrap;
	margin-bottom: 2rem;
	margin-left: ${marginLeft};
`
export const Tag = styled.p`
	border-radius: ${({ theme }) => theme.borderRadius};
	padding: 3px 12px;
	text-align: center;
	font-size: ${({ theme }) => theme.fontSize.xs};
	margin-right: 0.5rem;
	background-color: ${({ theme }) => theme.brand.primary};
`
export const Date = styled(NumberedHeading)`
	margin: 0;
	margin-left: ${marginLeft};
	color: ${({ theme }) => theme.brand.primary};
	font-size: ${({ theme }) => theme.fontSize.md};
	font-weight: ${({ theme }) => theme.fontw.bold};
	&:before {
		counter-increment: none;
		content: '';
		margin: 0;
	}
	&:after {
		width: 100%;
	}
`
export const PaginateBlog = styled(ReactPaginate).attrs({
	// You can redefine classes here, if you want.
	activeClassName: 'active',
})`
	margin-bottom: 2rem;
	display: flex;
	flex-direction: row;
	justify-content: space-around;
	list-style-type: none;
	padding: 0 5rem;
	li a {
		border-radius: 7px;
		padding: 0.1rem 1rem;
		border: ${({ theme }) => theme.borders.default + ' ' + theme.text.accent};
		border-radius: ${({ theme }) => theme.borderRadius};
		cursor: pointer;
	}
	li.previous a,
	li.next a,
	li.break a {
		border-color: none;
	}
	li.active a {
		background-color: ${({ theme }) => theme.brand.primary};
		border-color: none;
		color: white;
		min-width: 32px;
	}
	li.disabled a {
		color: grey;
	}
	li.disable,
	li.disabled a {
		cursor: default;
	}
`
