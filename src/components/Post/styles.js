import styled from 'styled-components'

export const StyledMainContainer = styled.section`
	width: 100%;
	max-width: 900px;
	min-height: 900px;
	counter-reset: section;
	margin-bottom: 15rem;
	section {
		margin: 0 auto;
		padding: 100px 0;
	}
`
