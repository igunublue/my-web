import ReactMarkdown from 'react-markdown'

import { StyledMainContainer } from './styles'

export default function Post({ content }) {
	return (
		<StyledMainContainer>
			<ReactMarkdown>{content}</ReactMarkdown>
		</StyledMainContainer>
	)
}
