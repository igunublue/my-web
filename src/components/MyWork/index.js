import { useEffect, useRef } from 'react'

import { Icon } from '@/components/Icons'
import { NumberedHeading } from '@/common/styles'
import { srConfig } from '@/config/sr'
import {
	StyledProject,
	StyledProjectLinks,
	StyledProjectImgWrapper,
	StyledProjectImage,
} from './styles'
import { Element } from 'react-scroll'
import config from '@/config'

const MyWork = () => {
	const { navLinkNames, myWork } = config
	const revealTitle = useRef(null)
	const revealProjects = useRef([])

	useEffect(() => {
		const ScrollReveal = require('scrollreveal')
		const sr = ScrollReveal.default()
		sr.reveal(revealTitle.current, srConfig())
		revealProjects.current.forEach((ref, i) =>
			sr.reveal(ref, srConfig(i * 100)),
		)
	}, [])

	return (
		<Element name={navLinkNames.myWork}>
			<section>
				<NumberedHeading ref={revealTitle}>I’ve Built:</NumberedHeading>
				<div>
					{myWork &&
						myWork.map((project, i) => {
							const { title, external, techs, github, cover, descriptionHtml } =
								project
							return (
								<StyledProject
									key={title}
									ref={(el) => (revealProjects.current[i] = el)}
								>
									<div className='project-content'>
										<p className='project-overline'>Work</p>
										<h3 className='project-title'>{title}</h3>
										<div
											className='project-description'
											dangerouslySetInnerHTML={{ __html: descriptionHtml }}
										/>
										<p className='project-overline'>Technologies</p>
										{techs.length && (
											<ul className='project-tech-list'>
												{techs.map((tech) => (
													<li key={tech}>{tech}</li>
												))}
											</ul>
										)}
										<StyledProjectLinks>
											{github && (
												<a
													rel='noreferrer'
													target='_blank'
													href={github}
													aria-label='GitHub Link'
												>
													<Icon name='GitHub' />
												</a>
											)}
											{external && (
												<a
													rel='noreferrer'
													target='_blank'
													href={external}
													aria-label='External Link'
												>
													<Icon name='External' />
												</a>
											)}
										</StyledProjectLinks>
									</div>
									<StyledProjectImgWrapper>
										<a href={external || github || '#'}>
											<div className='img-wrapper'>
												<div className='img-cont' />
												<StyledProjectImage
													src={cover}
													alt={title}
													className='img'
												/>
											</div>
										</a>
									</StyledProjectImgWrapper>
								</StyledProject>
							)
						})}
				</div>
			</section>
		</Element>
	)
}

export default MyWork
