import { forwardRef } from 'react'
import { withTheme } from 'styled-components'
import PropTypes from 'prop-types'

const IconLogo = forwardRef(function Logo(
	{ theme, width = 36.581, height = 50.186 },
	ref,
) {
	const fontFamily = 'PTMono-Regular'

	return (
		<svg
			id='logo'
			width={width}
			height={height}
			version='1.1'
			viewBox='0 0 500 500'
			xmlns='http://www.w3.org/2000/svg'
			ref={ref}
		>
			<title>Lenin Garizabalo logo</title>
			<g fill={theme.brand.primary}>
				<circle
					cx='250'
					cy='250'
					r='237.69'
					fill='#66a2ba'
					stroke='#263d4a'
					strokeWidth='13'
				/>
				<path
					d='m392.17 281.79c0.42239-0.87727 0.68234-1.8548 0.68234-2.8945l2e-3 -156.26c0-3.6987-2.992-6.6962-6.6962-6.6962h-272.34c-3.6987 0-6.6961 2.992-6.6961 6.6962v156.25c0 1.0533 0.25182 2.0226 0.68235 2.8945l-32.49 2e-3v10.812c0 3.6825 3.0001 6.6962 6.7042 6.6962h335.95c3.6987 0 6.707-3.0001 6.707-6.6962v-10.812zm-142.18-162.23c1.4568 0 2.6427 1.1832 2.6427 2.64 0 1.4595-1.1886 2.64-2.6427 2.64-1.4595 0-2.6427-1.1805-2.6427-2.64-2e-3 -1.4595 1.1832-2.64 2.6427-2.64zm-131.42 9.2982h262.84v141.95h-262.84zm150.76 161.69c0 1.4676-1.1832 2.6508-2.64 2.6508h-33.41c-1.4568 0-2.64-1.1832-2.64-2.6508v-6e-3c0-1.4568 1.1832-2.64 2.64-2.64h33.41c1.4568 0 2.64 1.1832 2.64 2.64z'
					fill='#263d4a'
				/>
				<g fill='#263d4a' strokeWidth='.78511'>
					<path d='m169.71 205.14 54.774 25.445v-12.092l-41.427-18.143v-0.23l41.427-18.143v-12.102l-54.774 25.449z' />
					<path d='m232.95 235.15h11.525l24.421-82.735h-11.529z' />
					<path d='m275.51 181.98 42.344 18.143v0.23001l-42.344 18.143v12.092l54.78-25.102v-10.498l-54.78-25.111z' />
				</g>
				<g transform='translate(1.241)' fill='#263d4a'>
					<text
						x='167.37346'
						y='428.70361'
						fontFamily={fontFamily}
						fontSize='139.71px'
						stroke='#263d4a'
						strokeWidth='.72764'
						style={{ lineHeight: '1.25' }}
						xmlSpace='preserve'
					>
						<tspan
							x='167.37346'
							y='428.70361'
							fill='#263d4a'
							fontSize='139.71px'
							stroke='#263d4a'
							strokeWidth='.72764'
						>
							L
						</tspan>
					</text>
					<text
						x='245.86102'
						y='426.64865'
						fontFamily={fontFamily}
						fontSize='134.56px'
						stroke='#263d4a'
						strokeWidth='.70086'
						style={{ lineHeight: '1.25' }}
						xmlSpace='preserve'
					>
						<tspan
							x='245.86102'
							y='426.64865'
							fill='#263d4a'
							fontSize='134.56px'
							stroke='#263d4a'
							strokeWidth='.70086'
						>
							G
						</tspan>
					</text>
					<circle cx='252.87' cy='423.26' r='5.3688' />
				</g>
			</g>
		</svg>
	)
})

IconLogo.propTypes = {
	theme: PropTypes.object,
	width: PropTypes.number,
	height: PropTypes.number,
}

export default withTheme(IconLogo)
