import { useState, useEffect } from 'react'
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import { Element } from 'react-scroll'

import config from '@/config'
import { NAV_DELAY, LOADER_DELAY } from '@/lib/constants'
import { StyledHeroSection, StyledBigTitle } from './styles'

const Hero = () => {
	const { email } = config
	const [isMounted, setIsMounted] = useState(false)

	useEffect(() => {
		const timeout = setTimeout(() => setIsMounted(true), NAV_DELAY)
		return () => clearTimeout(timeout)
	}, [])

	const one = <h1>Welcome, I&apos;m</h1>
	const two = <StyledBigTitle>Lenin Garizabalo.</StyledBigTitle>
	const three = <StyledBigTitle slate>I build web apps.</StyledBigTitle>
	const four = (
		<p>
			I&apos;m a software developer based in Barranquilla, Colombia,
			specializing in building exceptional websites applications, and everything
			in between.
		</p>
	)
	const five = (
		<a href={`mailto:${email}`} className='email-link'>
			Get In Touch
		</a>
	)

	const items = [one, two, three, four, five]

	return (
		<Element name='hero'>
			<StyledHeroSection>
				<TransitionGroup component={null}>
					{isMounted &&
						items.map((item, i) => (
							<CSSTransition key={i} classNames='fadeup' timeout={LOADER_DELAY}>
								<div style={{ transitionDelay: `${i + 1}00ms` }}>{item}</div>
							</CSSTransition>
						))}
				</TransitionGroup>
			</StyledHeroSection>
		</Element>
	)
}

export default Hero
