/* eslint-disable global-require */
import { useEffect, useRef } from 'react'
import Image from 'next/image'
import { Element } from 'react-scroll'

import { NumberedHeading } from '@/common/styles'
import { srConfig } from '@/config/sr'
import { StyledAboutSection, StyledText, StyledPic } from './styles'
import config from '@/config'

function AboutMe() {
	const { navLinkNames, skills } = config
	const revealContainer = useRef(null)

	useEffect(() => {
		const ScrollReveal = require('scrollreveal')
		const sr = ScrollReveal.default()
		sr.reveal(revealContainer.current, srConfig())
	}, [])

	return (
		<Element name={navLinkNames.aboutMe}>
			<StyledAboutSection ref={revealContainer}>
				<NumberedHeading>About Me</NumberedHeading>
				<div className='inner'>
					<StyledText>
						<div>
							<p>
								Hello! I&apos;m Lenin, a Full stack developer based in
								Barranquilla, Colombia.
							</p>
							<p>
								I enjoy creating beautiful and reliable applications for
								internet.
								<br />
								My goal is to always build scalable products and performant
								experiences.
							</p>
							<br />
							<p>
								Here are a few technologies I&apos;ve been working with
								recently:
							</p>
						</div>

						<ul className='skills-list'>
							{skills && skills.map((skill) => <li key={skill}>{skill}</li>)}
						</ul>
					</StyledText>
					<StyledPic>
						<div className='wrapper'>
							<Image
								width={300}
								height={300}
								src='/avatar.jpeg'
								alt='Avatar'
								className='img'
							/>
						</div>
					</StyledPic>
				</div>
			</StyledAboutSection>
		</Element>
	)
}

export default AboutMe
