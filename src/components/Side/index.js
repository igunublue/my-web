import { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { CSSTransition, TransitionGroup } from 'react-transition-group'

import { LOADER_DELAY } from '@/lib/constants'
import { StyledSideElement } from './styles'

const Side = ({ children, isHome, orientation }) => {
	const [isMounted, setIsMounted] = useState(!isHome)

	useEffect(() => {
		let timeout

		if (isHome) {
			timeout = setTimeout(() => setIsMounted(true), LOADER_DELAY)
		}

		return () => clearTimeout(timeout)
	}, [isHome])

	return (
		<StyledSideElement orientation={orientation}>
			<TransitionGroup component={null}>
				{isMounted && (
					<CSSTransition
						classNames={isHome ? 'fade' : ''}
						timeout={isHome ? LOADER_DELAY : 0}
					>
						{children}
					</CSSTransition>
				)}
			</TransitionGroup>
		</StyledSideElement>
	)
}

Side.propTypes = {
	children: PropTypes.node.isRequired,
	isHome: PropTypes.bool,
	orientation: PropTypes.string,
}

export default Side
