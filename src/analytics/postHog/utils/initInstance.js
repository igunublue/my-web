import posthog from 'posthog-js'

import config from '../../../config'
import constants from '../constants'

export default function initInstance() {
	if (typeof window !== 'undefined') {
		if (
			!window.location.href.includes('127.0.0.1') ||
			!window.location.href.includes('localhost')
		) {
			const { envKeys } = config
			posthog.init(envKeys.analytics.POST_HOST_KEY, {
				api_host: constants.host,
			})
		}
	}
}
