import common from './common'

const lightTheme = {
	...common,
	bg: {
		default: '#66A2BA',
		defaultLight: '#66A2BA',
		reverse: '#F4F4F4',
	},
	text: {
		default: '#F4F4F4',
		reverse: '#0A1A2F',
		accent: '#274a5e',
	},
	shadows: {
		// default: '0 10px 30px -10px rgba(2, 12, 27, 0.7)',
		default:
			'0px 3px 3px -2px rgb(0 0 0 / 20%), 0px 3px 4px 0px rgb(0 0 0 / 14%), 0px 1px 8px 0px rgb(0 0 0 / 12%)',
		small: '0 10px 30px -10px rgba(2, 12, 27, 0.7)',
		medium: '0 20px 30px -15px rgba(2,12,27, 0.7)',
		large: '0 30px 60px rgba(0, 0, 0, 0.12) ',
	},
	borders: {
		default: '1px solid',
	},
}

export default lightTheme
