const envKeys = {
	analytics: {
		POST_HOST_KEY: process.env.NEXT_PUBLIC_POST_HOST_KEY,
	},
}

export default envKeys
