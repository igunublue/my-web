/* eslint-disable max-len */
import myWork from './myWork'
import projects from './projects'
import envKeys from './envKeys'

const navLinkNames = {
	aboutMe: 'About me',
	blog: 'Blog',
	myWork: 'My work',
	projects: 'Projects',
	contact: 'Contact',
}

const config = {
	envKeys,
	email: 'lcamargo@protonmail.com',
	myWork,
	projects,
	skills: ['JavaScript', 'TypeScript', 'React', 'nodeJs', 'Next.js', 'NestJs'],
	socialMedia: [
		{
			name: 'GitLab',
			url: 'https://gitlab.com/IGUNUBLUE',
		},
		{
			name: 'GitHub',
			url: 'https://github.com/IGUNUBLUE',
		},
		{
			name: 'Linkedin',
			url: 'https://www.linkedin.com/in/lenin-garizabalo/',
		},
	],
	navLinkNames,
	navLinks: [
		{
			name: navLinkNames.aboutMe,
			url: '/#about-me',
		},
		{
			name: navLinkNames.myWork,
			url: '/#my-work',
		},
		{
			name: navLinkNames.projects,
			url: '/#projects',
		},
		// {
		// 	name: navLinkNames.blog,
		// 	url: '/#blog',
		// },
		{
			name: navLinkNames.contact,
			url: '/#contact',
		},
	],
	colors: {
		green: '#64ffda',
		navy: '#0a192f',
		darkNavy: '#020c1b',
	},
}

export default config
