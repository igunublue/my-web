/* eslint-disable max-len */
module.exports = [
	{
		title: 'Fundraiser program',
		cover: '/images/featured/cr_at_sundevs2.jpg',
		external: 'https://www.caferio.com/fundraiser',
		descriptionHtml:
			'This page was created for one of our clients in the company <a target="_blank" rel="links" href="https://www.sundevs.com/">sundevs.com</a>.',
		techs: ['nextJs', 'HTML5', 'CSS'],
	},
	{
		title: 'Ambassador program',
		cover: '/images/featured/cr_at_sundevs1.jpg',
		external: 'https://www.caferio.com/ambassador-program',
		descriptionHtml:
			'This page was created for one of our clients in the company <a target="_blank" rel="links" href="https://www.sundevs.com/">sundevs.com</a>.',
		techs: ['nextJs', 'HTML5', 'CSS'],
	},
]
