const projects = [
	{
		title: 'Word Scrambler',
		external: 'https://word-scrambler.vercel.app/',
		github: 'https://github.com/IGUNUBLUE/word-scrambler',
		descriptionHtml:
			'Word scramble is a game where you must guess the sentence and fill in each space with the corresponding letter to move on to the next one.',
		techs: ['React', 'Redux', 'SASS', 'Axios', 'Atomic Design', 'Hooks'],
	},
	{
		title: 'Characters',
		external: 'https://characters-plum.vercel.app/',
		github: 'https://github.com/IGUNUBLUE/Characters',
		descriptionHtml:
			'Front-end to display character profiles with search functions, the option to add tags, show and hide ratings.',
		techs: ['React', 'Redux', 'CSS', 'Axios', 'Hooks'],
	},
	{
		title: 'Digital Art | ARCHIVED',
		github: 'https://github.com/IGUNUBLUE/digital-art',
		descriptionHtml:
			'Final project, bootcamp: <a target="_blank" rel="links" href="https://www.soyhenry.com/">HENRY</a>. DEVs: <a target="_blank" rel="Lucia Llorca" href="https://github.com/Lls28es">Lucia Llorca</a>, <a target="_blank" rel="Angel Zarate" href="https://github.com/Andromata">Angel Zarate</a>, <a target="_blank" rel="Dario Elguero" href="https://github.com/Dario-Elguero">Dario Elguero</a>, <a target="_blank" rel="Ezequiel Rivas" href="https://github.com/CheizeX">Ezequiel Rivas</a>, <a target="_blank" rel="Cristian Hernandez" href="https://github.com/cristian-hr">Cristian Hernandez</a>, <a target="_blank" rel="Nahuel Rojas" href="https://github.com/nahwish">Nahuel Rojas</a>, <a target="_blank" rel="Santiago Ferrera" href="https://github.com/santiago663">Santiago Ferrera</a> and me.<br></br>Digital art is an online art gallery where artists can publish, sell digital copies and create a community around their work.',
		techs: [
			'React',
			'Redux',
			'SASS',
			'Axios',
			'Hooks',
			'Postgres',
			'Sequelize',
			'FireBase',
			'NodeJs',
			'Heroku',
			'Twilio',
			'Stripe',
			'Paypal',
		],
	},
]

export default projects
